﻿using System;
using System.Numerics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CalculatorWPF
{
    public partial class MainWindow : Window
    {
        private double currentNumber = 0;
        private string currentOperation = "";
        private string output = "";
        private bool resultDisplayed = false;
        private bool useDegrees = false;
        private bool lastInputWasNumber = false;
        private bool isMemoryListBoxExpanded = false;
        private readonly bool eEntered = false;
        private readonly double originalFontSize;
        private const double Pi = Math.PI;
        private const double E = Math.E;
        private double memory = 0;
        private List<MemoryItem> memoryList = new List<MemoryItem>();
        public MainWindow()
        {
            InitializeComponent();
            originalFontSize = outputTextBlock.FontSize;
            DataContext = this;
            PreviewKeyDown += MainWindowPreviewKeyDown;
            MemoryListBox.ItemsSource = memoryList;
        }
        private void MainWindowPreviewKeyDown(object sender, KeyEventArgs e)
        {
            bool shiftPressed = Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);

            switch (e.Key)
            {
                case Key.D0:
                case Key.NumPad0:
                    NumBtnClickWithKeyboard("0");
                    break;
                case Key.D1:
                case Key.NumPad1:
                    if (shiftPressed)
                    {
                        FactorialBtnClick(this, new RoutedEventArgs());
                    }
                    else
                    {
                        NumBtnClickWithKeyboard("1");
                    }
                    break;
                case Key.D2:
                case Key.NumPad2:
                    NumBtnClickWithKeyboard("2");
                    break;
                case Key.D3:
                case Key.NumPad3:
                    NumBtnClickWithKeyboard("3");
                    break;
                case Key.D4:
                case Key.NumPad4:
                    NumBtnClickWithKeyboard("4");
                    break;
                case Key.D5:
                case Key.NumPad5:
                    if (shiftPressed)
                    {
                        ModBtnClick(this, new RoutedEventArgs());
                    }
                    else
                    {
                        NumBtnClickWithKeyboard("5");
                    }
                    break;
                case Key.D6:
                case Key.NumPad6:
                    if (shiftPressed)
                    {
                        PowerBtnClick(this, new RoutedEventArgs());
                    }
                    else
                    {
                        NumBtnClickWithKeyboard("6");
                    }
                    break;
                case Key.D7:
                case Key.NumPad7:
                    NumBtnClickWithKeyboard("7");
                    break;
                case Key.D8:
                case Key.NumPad8:
                    if (shiftPressed)
                    {
                        TimesBtnClick(this, new RoutedEventArgs());
                    }
                    else
                    {
                        NumBtnClickWithKeyboard("8");
                    }
                    break;
                case Key.D9:
                case Key.NumPad9:
                    NumBtnClickWithKeyboard("9");
                    break;
                case Key.Add:
                case Key.OemPlus:
                    if (shiftPressed)
                    {
                        PlusBtnClick(this, new RoutedEventArgs());
                    }
                    else
                    {
                        EqualsBtnClick(this, new RoutedEventArgs());
                    }
                    break;
                case Key.Subtract:
                case Key.OemMinus:
                    MinusBtnClick(this, new RoutedEventArgs());
                    break;
                case Key.Divide:
                case Key.OemQuestion:
                case Key.Oem5:
                    DivideBtnClick(this, new RoutedEventArgs());
                    break;
                case Key.Multiply:
                    TimesBtnClick(this, new RoutedEventArgs());
                    break;
                case Key.Decimal:
                    DecimalBtnClick(this, new RoutedEventArgs());
                    break;
                case Key.Enter:
                    EqualsBtnClick(this, new RoutedEventArgs());
                    break;
                case Key.Back:
                    BackspaceBtnClick(this, new RoutedEventArgs());
                    break;
                case Key.Escape:
                    ClearEntryBtnClick(this, new RoutedEventArgs());
                    break;
                default:
                    break;
            }
        }
        private void NumBtnClickWithKeyboard(string number)
        {
            if (resultDisplayed || !lastInputWasNumber)
            {
                output = "0";
                resultDisplayed = false;
                currentOperation = "";
                currentOperationTextBlock.Text = "";
                outputTextBlock.Text = "";
            }

            if (!eEntered && output != null && output.Length < 32)
            {
                output = output == "0" || currentOperationTextBlock.Text == outputTextBlock.Text ? number : output + number;
                outputTextBlock.Text = output;
                UpdateFontSize();
                EnableAllButtonsAndDigits();
                TenPowerBtn.IsEnabled = true;
                lastInputWasNumber = true;
            }
        }
        private void DisableAllButtonsExceptCEAndDigits()
        {
            foreach (UIElement element in MainGrid.Children)
            {
                if (element is Button button)
                {
                    string[] allowedCharacters = { "CE", ".", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

                    if (!allowedCharacters.Contains(button.Content.ToString()))
                    {
                        button.IsEnabled = false;
                    }
                }
            }
        }
        private void EnableAllButtonsAndDigits()
        {
            foreach (UIElement element in MainGrid.Children)
            {
                if (element is Button button && button.Content is string content && !char.IsDigit(content[0]))
                {
                    button.IsEnabled = true;
                }
            }
        }
        private void EnableAllButtons()
        {
            foreach (UIElement element in MainGrid.Children)
            {
                if (element is Button button)
                {
                    button.IsEnabled = true;
                }
            }
        }
        private void UpdateFontSize()
        {
            const double maxFontSize = 50;
            const double minFontSize = 20;

            outputTextBlock.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            double textWidth = outputTextBlock.DesiredSize.Width;

            if (textWidth > outputTextBlock.ActualWidth)
            {
                double newFontSize = outputTextBlock.FontSize * outputTextBlock.ActualWidth / textWidth;
                outputTextBlock.FontSize = Math.Max(Math.Min(newFontSize, maxFontSize), minFontSize);
            }
            else if (textWidth < outputTextBlock.ActualWidth && outputTextBlock.FontSize < originalFontSize)
            {
                outputTextBlock.FontSize = Math.Min(originalFontSize, outputTextBlock.FontSize + 0.65);
            }
        }
        private void HandleResultModification(bool resultModified)
        {
            if (!resultModified)
            {
                output = "";
                outputTextBlock.Text = output;
            }
            resultDisplayed = resultModified;
        }
        private static double ParseOutputToDouble(string input)
        {
            return double.TryParse(input, out double result) ? result : 0;
        }
        private void DisplayOutput(string output)
        {
            this.output = output;
            outputTextBlock.Text = output;
            HandleResultModification(true);
        }
        private void DisplayErrorMessage(string errorMessage)
        {
            DisplayOutput(errorMessage);

            if (errorMessage == "Error: Invalid input" || errorMessage == "Error: Overflow")
            {
                DisableAllButtonsExceptCEAndDigits();
            }
            else
            {
                EnableAllButtonsAndDigits();
            }

        }
        private void CalculateResult(double result = 0)
        {
            double secondNumber = ParseOutputToDouble(output);

            switch (currentOperation)
            {
                case "Minus":
                    result = currentNumber - secondNumber;
                    break;
                case "Plus":
                    result = currentNumber + secondNumber;
                    break;
                case "Times":
                    result = currentNumber * secondNumber;
                    break;
                case "Exp":
                    result = Math.Exp(currentNumber);
                    break;
                case "Power":
                    result = Math.Pow(currentNumber, secondNumber);
                    break;
                case "Pi":
                    result = currentNumber * Pi;
                    break;
                case "Divide":
                    if (secondNumber != 0)
                        result = currentNumber / secondNumber;
                    else
                        output = "Error: Division by zero";
                    break;
                case "Mod":
                    if (secondNumber != 0)
                        result = currentNumber % secondNumber;
                    else
                        output = "Error: Division by zero";
                    break;
            }

            DisplayOutput(result.ToString().Length > 16 ? result.ToString("E") : result.ToString());

        }
        private void NumBtnClick(object sender, RoutedEventArgs e)
        {
            if (resultDisplayed || !lastInputWasNumber)
            {
                output = "";
                resultDisplayed = false;
                currentOperation = "";
                currentOperationTextBlock.Text = "";
                outputTextBlock.Text = "";
            }
            string? bc = ((Button)sender)?.Content?.ToString();
            if (!string.IsNullOrWhiteSpace(bc))
            {
                if (output == "0")
                {
                    output = bc;
                    outputTextBlock.Text = output;
                }
                else if (output != null && output.Length < 32)
                {
                    output += bc;
                    outputTextBlock.Text = output;
                }
                UpdateFontSize();
                EnableAllButtonsAndDigits();
                TenPowerBtn.IsEnabled = true;
                lastInputWasNumber = true;
            }
        }
        private void HandleOperatorButtonClick(string operation)
        {
            currentOperationTextBlock.Text = "";

            currentNumber = !string.IsNullOrEmpty(output) ? ParseOutputToDouble(output) : currentNumber;
            output = string.Empty;
            currentOperation = operation;

            string currentOperationText = $"{currentNumber}";

            if (currentNumber < 0)
            {
                currentOperationText = $"({currentOperationText})";
            }

            currentOperationTextBlock.Text += currentOperationText + GetOperationSymbol(operation);

            outputTextBlock.Text = "";
            resultDisplayed = false;
        }
        private string GetOperationSymbol(string operation)
        {
            switch (operation)
            {
                case "Plus":
                    return " +";
                case "Minus":
                    return " -";
                case "Times":
                    return " ×";
                case "Divide":
                    return " ÷";
                case "Mod":
                    return " mod";
                case "Power":
                    return " ^";
                case "Exp":
                    return "e";
                default:
                    return "";
            }
        }
        private void OperatorBtnClick(object sender, RoutedEventArgs e)
        {
            lastInputWasNumber = false;
            HandleOperatorButtonClick(((Button)sender).Name);
        }
        private void EqualsBtnClick(object sender, RoutedEventArgs e)
        {
            if (currentOperationTextBlock.Text == "")
            {
                return;
            }

            if (!string.IsNullOrEmpty(currentOperationTextBlock.Text))
            {
                if (currentOperationTextBlock.Text.Length >= 2)
                {
                    currentOperationTextBlock.Text = currentOperationTextBlock.Text.Remove(currentOperationTextBlock.Text.Length - 2, 2);
                }
                else
                {
                    currentOperationTextBlock.Text = "";
                }
            }
            string resultText = output;
            double secondNumber = ParseOutputToDouble(output);
            if (secondNumber < 0)
            {
                resultText = $"({secondNumber})";
            }

            if (currentOperation == "Power")
            {
                currentOperationTextBlock.Text += $" ^ {output}";
            }
            else
            {
                currentOperationTextBlock.Text += $"{GetOperationSymbol(currentOperation)} {resultText}";
            }

            currentOperationTextBlock.Text += " = ";

            CalculateResult();
            UpdateFontSize();
        }
        private void ClearEntryBtnClick(object sender, RoutedEventArgs e)
        {
            output = string.Empty;
            outputTextBlock.Text = output;
            currentOperation = string.Empty;
            currentOperationTextBlock.Text = "";
            outputTextBlock.FontSize = originalFontSize;
            resultDisplayed = false;
            EnableAllButtons();
        }
        private void MinusBtnClick(object sender, RoutedEventArgs e) => HandleOperatorButtonClick("Minus");

        private void PlusBtnClick(object sender, RoutedEventArgs e) => HandleOperatorButtonClick("Plus");

        private void TimesBtnClick(object sender, RoutedEventArgs e) => HandleOperatorButtonClick("Times");

        private void DivideBtnClick(object sender, RoutedEventArgs e) => HandleOperatorButtonClick("Divide");

        private void BackspaceBtnClick(object sender, RoutedEventArgs e)
        {
            if (!resultDisplayed && !string.IsNullOrEmpty(output))
            {
                output = output.Remove(output.Length - 1);
                outputTextBlock.Text = output;
                UpdateFontSize();
            }
        }
        private void DecimalBtnClick(object sender, RoutedEventArgs e)
        {
            if (!output.Contains(','))
            {
                output += ",";
                outputTextBlock.Text = output;
            }
        }
        private void SignChangeBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output))
            {
                double number = ParseOutputToDouble(output);
                number = -number;
                output = number.ToString();
                outputTextBlock.Text = output;
                UpdateFontSize();
            }
        }
        private void SquareRootBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output))
            {
                double number = ParseOutputToDouble(output);
                if (number >= 0)
                {
                    double result = Math.Sqrt(number);
                    string operationText = $"√({number})";
                    string resultText = result.ToString();
                    currentOperationTextBlock.Text = operationText + " = ";
                    DisplayOutput(resultText);
                }
                else
                {
                    DisplayErrorMessage("Error: Invalid input");
                }
            }
        }
        private void SquareBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output) && double.TryParse(output, out double number))
            {
                string operationText = $"({number})^2 =";
                currentOperationTextBlock.Text = operationText;
                if (number >= -999999999 && number <= 999999999)
                {
                    DisplayOutput(Math.Pow(number, 2).ToString());
                }
                else
                {
                    DisplayErrorMessage("Overflow");
                }
            }
            else
            {
                DisplayErrorMessage("Error: Invalid input");
            }
        }
        private void TenPowerBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output) && double.TryParse(output, out double exponent))
            {
                const double maxExponent = 9999;

                string operationText = $"10^{exponent} = ";
                currentOperationTextBlock.Text = operationText;

                if (exponent <= maxExponent)
                {
                    BigInteger result = BigInteger.Pow(10, (int)exponent);

                    DisplayOutput(result.ToString().Length > 16 ? result.ToString("E") : result.ToString());
                }
                else
                {
                    DisplayErrorMessage("Error: Overflow");
                }
            }
            else
            {
                DisplayErrorMessage("Error: Invalid input");
            }
            UpdateFontSize();
        }
        private void LogBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output) && double.TryParse(output, out double number))
            {
                string operationText = $"log({number}) = ";
                currentOperationTextBlock.Text = operationText;

                if (number > 0)
                {
                    DisplayOutput(Math.Log10(number).ToString());
                }
                else
                {
                    DisplayErrorMessage("Error: Overflow");
                }
            }
            else
            {
                DisplayErrorMessage("Error: Invalid input");
            }
            UpdateFontSize();
        }
        private void LnBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output) && double.TryParse(output, out double number))
            {
                string operationText = $"ln({number}) = ";
                currentOperationTextBlock.Text = operationText;

                if (number > 0)
                {
                    DisplayOutput(Math.Log(number).ToString());
                }
                else
                {
                    DisplayErrorMessage("Error: Overflow");
                }
            }
            else
            {
                DisplayErrorMessage("Error: Invalid input");
            }
            UpdateFontSize();
        }
        private void FactorialBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output) && int.TryParse(output, out int number))
            {
                string operationText = $"{number}! = ";
                currentOperationTextBlock.Text = operationText;

                if (number >= 0 && number <= 3249)
                {
                    BigInteger result = 1;
                    for (int i = 2; i <= number; i++)
                    {
                        result *= i;
                    }

                    DisplayOutput(result.ToString().Length > 32 ? result.ToString("E") : result.ToString());
                }
                else if (number > 3249)
                {
                    DisplayErrorMessage("Overflow");
                }
                else
                {
                    DisplayErrorMessage("Error: Invalid input");
                }
            }
            else
            {
                DisplayErrorMessage("Error: Invalid input");
            }
            UpdateFontSize();
        }
        private void ModBtnClick(object sender, RoutedEventArgs e) => HandleOperatorButtonClick("Mod");

        private void PowerBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output))
            {
                double number = ParseOutputToDouble(output);

                if (output.Length > 12)
                {
                    DisplayErrorMessage("Error: Invalid input");
                    return;
                }

                currentOperationTextBlock.Text += output + " ^";
                currentNumber = number;
                output = "";
                currentOperation = "Power";
            }
            else
            {
                DisplayErrorMessage("Error: Invalid input");
                return;
            }
        }
        private void ExpBtnClick(object sender, RoutedEventArgs e) => HandleOperatorButtonClick("Exp");

        private void InverseBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output) && double.TryParse(output, out double number))
            {
                string operationText = $"1 / ({number}) = ";
                currentOperationTextBlock.Text = operationText;
                const double epsilon = 1e-10;
                if (Math.Abs(number) > epsilon)
                {
                    double result = 1 / number;
                    DisplayOutput(result.ToString());
                }
                else
                {
                    DisplayErrorMessage("Error: Overflow");
                }
            }
            else
            {
                DisplayErrorMessage("Error: Invalid input");
            }
            UpdateFontSize();
        }
        private void AbsBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output))
            {
                double number = ParseOutputToDouble(output);
                string operationText = $"|{number}| =";
                currentOperationTextBlock.Text = operationText;
                double result = Math.Abs(number);
                DisplayOutput(result.ToString());
            }
        }

        private void MemoryPlusBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output))
            {
                double value = ParseOutputToDouble(output);
                memory += value;

                if (memoryList.Count > 0)
                {
                    memoryList[memoryList.Count - 1] = new MemoryItem(memory);
                    UpdateMemoryListBox();
                }
                else
                {
                    memoryList.Add(new MemoryItem(memory));
                    UpdateMemoryListBox();
                }

                DisplayOutput(memory.ToString());
                resultDisplayed = true;
            }
        }

        private void MemoryMinusBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output))
            {
                double value = ParseOutputToDouble(output);
                memory -= value;

                if (memoryList.Count > 0)
                {
                    memoryList[memoryList.Count - 1] = new MemoryItem(memory);
                    UpdateMemoryListBox();
                }
                else
                {
                    memoryList.Add(new MemoryItem(memory));
                    UpdateMemoryListBox();
                }

                DisplayOutput(memory.ToString());
                resultDisplayed = true;
            }
        }
        private void MemoryRecallBtnClick(object sender, RoutedEventArgs e)
        {
            if (memoryList.Count > 0)
            {
                DisplayOutput(memoryList[memoryList.Count - 1].Value.ToString());
            }
            resultDisplayed = true;
            UpdateFontSize();
        }
        private void MemoryClearBtnClick(object sender, RoutedEventArgs e)
        {
            memory = 0;
            memoryList.Clear();
            UpdateMemoryListBox();
            if (memoryList.Count == 0)
            {
                MemoryRecallBtn.Visibility = Visibility.Collapsed;
                MemoryClearBtn.Visibility = Visibility.Collapsed;
            }
        }
        private void MemorySaveBtnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(output))
            {
                double value = ParseOutputToDouble(output);
                memoryList.Add(new MemoryItem(value));
                UpdateMemoryListBox();
            }
        }
        private void HandleTrigFunctionClick(Func<double, double> trigFunction, string operationSymbol)
        {
            if (!string.IsNullOrEmpty(output) && double.TryParse(output, out double number))
            {
                double angle = number;
                if (useDegrees)
                {
                    angle = DegreesToRadians(angle);
                }
                double result = trigFunction(angle);
                DisplayOutput(result.ToString());
                UpdateFontSize();
                HandleResultModification(true);
                currentOperationTextBlock.Text = $"{operationSymbol}({output})";
            }
            else
            {
                DisplayErrorMessage("Error: Invalid input");
            }
        }
        private void SinMenuItemClick(object sender, RoutedEventArgs e) => HandleTrigFunctionClick(Math.Sin, "sin");
        private void CosMenuItemClick(object sender, RoutedEventArgs e) => HandleTrigFunctionClick(Math.Cos, "cos");
        private void TanMenuItemClick(object sender, RoutedEventArgs e) => HandleTrigFunctionClick(Math.Tan, "tan");
        private void ArcTanMenuItemClick(object sender, RoutedEventArgs e) => HandleTrigFunctionClick(Math.Atan, "arctan");
        private double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180.0;
        }
        private void TrigModeBtnClick(object sender, RoutedEventArgs e)
        {
            useDegrees = !useDegrees;
            TrigModeBtn.Content = useDegrees ? "Deg" : "Rad";
        }
        private void TrigSubMenuBtnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                ContextMenu contextMenu = TrigSubMenuBtn.ContextMenu;
                contextMenu.PlacementTarget = TrigSubMenuBtn;
                contextMenu.IsOpen = true;
                e.Handled = true;
            }
        }
        private void MemorySubMenuBtnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
                ContextMenu contextMenu = MemorySubMenuBtn.ContextMenu;
                contextMenu.PlacementTarget = MemorySubMenuBtn;
                contextMenu.IsOpen = true;
                if (memoryList.Count == 0)
                {
                    MemoryRecallBtn.Visibility = Visibility.Collapsed;
                    MemoryClearBtn.Visibility = Visibility.Collapsed;
                }
                else
                {
                    MemoryRecallBtn.Visibility = Visibility.Visible;
                    MemoryClearBtn.Visibility = Visibility.Visible;
                }
                e.Handled = true;
            }
        }
        private void PiBtnClick(object sender, RoutedEventArgs e)
        {
            DisplayOutput(Pi.ToString());
        }
        private void EBtnClick(object sender, RoutedEventArgs e)
        {
            DisplayOutput(E.ToString());
        }
        private void MemorySMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!isMemoryListBoxExpanded)
            { 
                for (int i = 0; i < 3; i++)
                {
                    ColumnDefinition column = new ColumnDefinition();
                    column.Width = new GridLength(1, GridUnitType.Star);
                    MainGrid.ColumnDefinitions.Add(column);
                }
                Width += 3 * (Width / MainGrid.ColumnDefinitions.Count);
                Grid.SetColumn(MemoryListBox, MainGrid.ColumnDefinitions.Count - 3);
                Grid.SetColumnSpan(MemoryListBox, 3);
                MemoryListBox.Visibility = Visibility.Visible;
                Grid.SetRowSpan(MemoryListBox, MainGrid.RowDefinitions.Count);
                MemoryListBox.VerticalAlignment = VerticalAlignment.Stretch;
                int currentColumnIndex = Grid.GetColumn(currentOperationTextBlock);
                Grid.SetColumn(currentOperationTextBlock, currentColumnIndex - 1);
                isMemoryListBoxExpanded = true;
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    MainGrid.ColumnDefinitions.RemoveAt(MainGrid.ColumnDefinitions.Count - 1);
                }
                Width -= 3 * (Width / MainGrid.ColumnDefinitions.Count);
                MemoryListBox.Visibility = Visibility.Collapsed;
                int currentColumnIndex = Grid.GetColumn(currentOperationTextBlock);
                Grid.SetColumn(currentOperationTextBlock, currentColumnIndex + 1);
                isMemoryListBoxExpanded = false;
            }
        }
        private void UpdateMemoryListBox()
        {
            MemoryListBox.ItemsSource = memoryList.Select(item => item.Value.ToString()).ToList();
        }
        public class MemoryItem
        {
            public double Value { get; set; }

            public MemoryItem(double value)
            {
                Value = value;
            }
        }
    }
}